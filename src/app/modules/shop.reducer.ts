import * as shopAction from "./shop.actions";

export function offerReducer (state: any = '', action: shopAction.actions) {
  switch(action.type) {
    case shopAction.GETOFFERS:
      return action.payload;
    default:
      return state;
  }
}

export function priceReducer (state: any = '', action: shopAction.actions) {
  switch(action.type) {
    case shopAction.SETPRICES:
      return action.payload;
    default:
      return state;
  }
}

export function chartReducer (state: any = '', action: shopAction.actions) {
  switch(action.type) {
    case shopAction.SETCHARTS:
      return action.payload;
    default:
      return state;
  }
}
