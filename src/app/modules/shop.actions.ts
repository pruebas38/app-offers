import { Action } from "@ngrx/store";

export const GETOFFERS = '[Offers] Get Offers';

export const SETPRICES = '[Prices] Set Prices';

export const SETCHARTS = '[Charts] Set Charts';

export class GetOffersAction implements Action {
  readonly type = GETOFFERS;

  constructor(public payload: {data: any}) {}
}

export class SetPriceAction implements Action {
  readonly type = SETPRICES;

  constructor(public payload: {data: any}) {}
}

export class SetChartsAction implements Action {
  readonly type = SETCHARTS;

  constructor(public payload: {data: any}) {}
}

export type actions = GetOffersAction | SetPriceAction | SetChartsAction;
