import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppOfferState } from '../../app.reducer';
import { SetChartsAction, SetPriceAction } from '../../shop.actions';

@Component({
  selector: 'app-offer',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.scss']
})
export class OfferComponent implements OnInit {

  public formOffer: FormGroup;
  offers: any;

  constructor(
    private formBuilder: FormBuilder,
    private store: Store<AppOfferState>
  ) {
    this.store.select('offer').subscribe( offers => {
      this.offers = offers;
    });
  }

  ngOnInit(): void {
    this.formOffer = this.formBuilder.group({
      data: [null, []],
    });
  }

  setOfferData(): void {
    const f: FormGroup = this.formOffer;
    const id = f.get('data')?.value;

    let itemData = this.offers.filter((data: any) => data.versions[0].id == id);

    const prices = itemData[0].versions[0].productOfferingPrices;

    const pricesAccion = new SetPriceAction(prices);
    this.store.dispatch(pricesAccion);

    const charts = itemData[0].versions[0].characteristics;

    const chartsAccion = new SetChartsAction(charts);
    this.store.dispatch(chartsAccion);
  }

}
