import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppOfferState } from '../../app.reducer';

@Component({
  selector: 'app-prices',
  templateUrl: './prices.component.html',
  styleUrls: ['./prices.component.scss']
})
export class PricesComponent implements OnInit {
  prices: any;

  constructor(
    private store: Store<AppOfferState>
  ) {
    this.store.select('price').subscribe( prices => {
      this.prices = prices;
    });
  }

  ngOnInit(): void {
  }

}
