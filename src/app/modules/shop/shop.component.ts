import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { DataService } from 'src/app/services/data.service';
import { AppOfferState } from '../app.reducer';
import { GetOffersAction } from '../shop.actions';


@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit {

  offerId: any;
  public offers: any;

  constructor(
    private store: Store<AppOfferState>,
    private dataService: DataService,
    ) { }

  ngOnInit(): void {
    this.getOffers();
  }

  private getOffers(): void {
    this.dataService.getOffers()
    .subscribe(response => {
      const offers: any = response;
      const accion = new GetOffersAction(offers);
      this.store.dispatch(accion);
    });

  }
}
