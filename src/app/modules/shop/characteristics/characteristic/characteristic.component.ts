import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-characteristic',
  templateUrl: './characteristic.component.html',
  styleUrls: ['./characteristic.component.scss']
})
export class CharacteristicComponent implements OnInit {
  @Input() characteristic: any;

  constructor() { }

  ngOnInit(): void {
  }

}
