import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppOfferState } from '../../app.reducer';

@Component({
  selector: 'app-characteristics',
  templateUrl: './characteristics.component.html',
  styleUrls: ['./characteristics.component.scss']
})
export class CharacteristicsComponent implements OnInit {
  characteristics: any;

  constructor(
    private store: Store<AppOfferState>
  ) {
    this.store.select('charts').subscribe( characteristics => {
      this.characteristics = characteristics;
    });
  }

  ngOnInit(): void {
  }

}
