import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShopComponent } from './shop.component';
import { ShopRoutingModule } from './shop-routing.module';
import { OfferComponent } from './offer/offer.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PricesComponent } from './prices/prices.component';
import { PriceComponent } from './prices/price/price.component';
import { CharacteristicsComponent } from './characteristics/characteristics.component';
import { CharacteristicComponent } from './characteristics/characteristic/characteristic.component';



@NgModule({
  declarations: [
    ShopComponent,
    OfferComponent,
    PricesComponent,
    PriceComponent,
    CharacteristicsComponent,
    CharacteristicComponent],
  imports: [
    CommonModule,
    ShopRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class ShopModule { }
