export interface AppOfferState {
  offer: string;
  price: string;
  charts: string;
}
